const fs = require("fs");
const path = require("path");

function createDir(path, callback) {
  fs.access(path, (err) => {
    if (err) {
      fs.mkdir(path, (err) => {
        if (err) {
          console.log(err);
        } else {
          console.log("Directory created");
          callback();
        }
      });
    } else {
      console.log("Directory already exists");
      callback();
    }
  });
}

function createFiles(filePath, callback) {
  for (let index = 0; index < filePath.length; index++) {
    fs.writeFile(filePath[index], "Hello world", (err) => {
      if (err) {
        console.log(err);
      } else {
        console.log(`file ${path.basename(filePath[index])} created`);
        if (index === filePath.length - 1) {
          callback();
        }
      }
    });
  }
}

function deleteFile(filePath) {
  for (let index = 0; index < filePath.length; index++) {
    fs.unlink(filePath[index], (err) => {
      if (err) {
        console.log(err);
      } else {
        console.log(`file ${path.basename(filePath[index])} deleted`);
      }
    });
  }
}

function dataInput(dirPath, fileNameArray) {
  createDir(dirPath, () => {
    createFiles(fileNameArray, () => {
      deleteFile(fileNameArray);
    });
  });
}

module.exports.dataInput = dataInput;
