const fs = require("fs");
const path = require("path");
const { deleteFile } = require("./problem1");

function readData(filePath, callback) {
  console.log("Reading...");
  fs.readFile(filePath, "utf-8", (err, data) => {
    if (err) {
      console.log(err);
    } else {
      console.log(`Finished reading ${path.basename(filePath)} \n`);
      callback(data);
    }
  });
}

function writeData(filePath, data, callback) {
  console.log("Writing...");
  fs.writeFile(filePath, data, (err) => {
    if (err) {
      console.log(err);
    } else {
      console.log(`Finished writing ${path.basename(filePath)} \n`);
      callback();
    }
  });
}

function appendNewFileName(pathToWrite, callback) {
  fileName = path.basename(pathToWrite);
  fs.appendFile("../filename.txt", `\n${fileName}`, (err) => {
    if (err) {
      console.log(err);
    } else {
      console.log(`${path.basename(pathToWrite)} added to filename.txt \n`);
      callback();
    }
  });
}

function deleteAllFile(patharray, callback) {
  for (let index = 0; index < patharray.length; index++) {
    if (patharray[index]) {
      fs.unlink(`../${patharray[index]}`, (err) => {
        if (err) {
          console.log(err);
        } else {
          console.log(`Deleted file ${path.basename(patharray[index])}`);
          if (index === patharray.length - 1){
              callback();
            }
        }
      });
    }
  }
}

function inputData(lipsumPath) {
  readData(lipsumPath, (data) => {
    let upperCaseData = data.toUpperCase();
    let upperlipsumPath = path.resolve(`${__dirname}`, "upperLipsum.txt");
    console.log(`lipsum data converting to upper case \n`);

    writeData(upperlipsumPath, upperCaseData, () => {
      writeData("../filename.txt", "upperLipsum.txt", () => {
        readData(upperlipsumPath, (data) => {
          let lowerCaseData = data.toLowerCase().split("\n").join(".");
          let lowerLipsumPath = path.resolve(`${__dirname}`, "lowerLipsum.txt");
          console.log(`converting data to lower case \n`);

          writeData(lowerLipsumPath, lowerCaseData, () => {
            appendNewFileName("lowerLipsum.txt",()=> {

              let combinedData = upperCaseData + "." + lowerCaseData;
              let dataArray = combinedData.split(".");
              let sortedData = dataArray.sort();
              sortedData = sortedData.join(".");
              let combinedlipsumPath = path.resolve(`${__dirname}`, "combinedLipsum.txt");

              writeData(combinedlipsumPath, sortedData, () => {
                appendNewFileName("combinedLipsum.txt", () => {
                  readData("../filename.txt", (data) => {
                    let fileNameArray = data.split("\n");
                    deleteAllFile(fileNameArray, () => {
                      console.log("all done");
                    });
                  });
                });
              });
            });
          });
        });
      });
    });
  });
}

module.exports.inputData = inputData;
