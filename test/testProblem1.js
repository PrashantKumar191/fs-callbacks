const path = require("path");

const problem1 = require("../problem1");

dirPath = path.resolve(`${__dirname}`, "./JSONoutputFiles");

let f1 = `${dirPath}/file1.json`;
let f2 = `${dirPath}/file2.json`;
let f3 = `${dirPath}/file3.json`;
let f4 = `${dirPath}/file4.json`;
let f5 = `${dirPath}/file5.json`;

let fileNameArray = [f1, f2, f3, f4, f5];

problem1.dataInput(dirPath, fileNameArray);
